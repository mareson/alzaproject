import {NextPage} from "next";
import {CategoryResponse, ProductResponse} from "../../apiProps/apiResponseTypes";
import CategoryPageComponent from "../../components/categories/CategoryPage";
import productService from "../../services/ProductService";
import categoryService from "../../services/CategoryService";
import Head from "next/head";

type PageProps = {
    productList: ProductResponse[],
    category?: CategoryResponse
}

const CategoryPage: NextPage<PageProps> = ({productList, category}) => {
    return (
        <>
            <Head>
                <title>{!category ? "Kategorie" : category.name}</title>
            </Head>
            {
                !!category && <CategoryPageComponent category={category} products={productList} />
            }
        </>
    );
};

export default CategoryPage;

export async function getServerSideProps({params}: {params: {slug: string}}): Promise<{
    props: PageProps;
}> {
    let productList: ProductResponse[] = [];
    let category: CategoryResponse | undefined = undefined;

    try {
        productList = await productService.retrieveProducts(); // TODO by category slug
        category = await categoryService.retrieveCategoryBySlug(params.slug);
    } catch (e) {}

    return  { props: { productList, category } };
}