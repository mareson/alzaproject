import Document, {Html, Head, Main, NextScript, DocumentContext} from "next/document";
import {ServerStyleSheet} from "styled-components";

export default class MyDocument extends Document {

    static async getInitialProps(ctx: DocumentContext) {
        // Next.js documentation – To prepare for React 18, we recommend avoiding customizing getInitialProps and renderPage, if possible.
        // TODO Unfortunately styled-components is not ready for this yet.

        const sheet = new ServerStyleSheet();
        const originalRenderPage = ctx.renderPage;

        try {
            ctx.renderPage = () =>
                originalRenderPage({
                    enhanceApp: (App) => (props) =>
                        sheet.collectStyles(<App {...props} />),
                });

            const initialProps = await Document.getInitialProps(ctx);
            return {
                ...initialProps,
                styles: [initialProps.styles, sheet.getStyleElement()],
            };
        } finally {
            sheet.seal();
        }
    }
}