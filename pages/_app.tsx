import type { AppProps } from 'next/app';
import GlobalStyle from "../components/GlobalStyle";
import {ThemeProvider} from "styled-components";
import {THEME} from "../theme";
import LoadingBar from "../components/LoadingBar";
import Footer from "../components/Footer";

function MyApp({ Component, pageProps }: AppProps) {

    return (
        <ThemeProvider theme={THEME}>
            <GlobalStyle />
            <LoadingBar>
                <Component {...pageProps} />
                <Footer />
            </LoadingBar>
        </ThemeProvider>
    );
}

export default MyApp
