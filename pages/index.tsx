import type {NextPage} from 'next';
import PageWrapper from "../components/PageWrapper";
import Link from "../components/Link";
import styled from "styled-components";
import Head from "next/head";

const Home: NextPage = () => {
  return (
      <StyledPageWrapper>
          <Head>
              <title>Úvodní strana</title>
          </Head>
          <Link href="/category/laptops">Přejít na stránku s notebooky (úkol)</Link>
      </StyledPageWrapper>
  );
};

export default Home;

const StyledPageWrapper = styled(PageWrapper)`
  padding-top: ${({theme})=>theme.spacing(3)};
`;