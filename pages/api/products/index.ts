import {NextApiRequest, NextApiResponse} from "next";
import basicHandler, {HttpMethod} from "../../../apiProps/apiHandler";
import {ApiError, handleError} from "../../../apiProps/apiErrors";
import alzaService from "../../../services/AlzaService";
import {ProductsResponse} from "../../../apiProps/apiResponseTypes";

async function handleGET(
    request: NextApiRequest,
    response: NextApiResponse<ProductsResponse>
) {
    try {
        const data = await alzaService.retrieveProducts();
        response.status(200).json(data);
    } catch (e) {
        const errorKey = e as ApiError;
        handleError(response, errorKey);
    }
}

const handler = (req: NextApiRequest, res: NextApiResponse)=>basicHandler(req, res, {
    [HttpMethod.GET]: handleGET
});
export default handler;