import {NextApiRequest, NextApiResponse} from "next";
import basicHandler, {HttpMethod} from "../../../apiProps/apiHandler";
import {CategoryResponse} from "../../../apiProps/apiResponseTypes";
import {ApiError, handleError} from "../../../apiProps/apiErrors";
import categoryService from "../../../services/CategoryService";

type RouteParams = {
    slug: string;
}

async function handleGET(
    request: NextApiRequest,
    response: NextApiResponse<CategoryResponse>
) {
    const {slug} = request.query as RouteParams;

    try {
        const data = await categoryService.retrieveCategoryBySlug(slug);
        response.status(200).json(data);
    } catch (e) {
        const errorKey = e as ApiError;
        handleError(response, errorKey);
    }
}

const handler = (req: NextApiRequest, res: NextApiResponse)=>basicHandler(req, res, {
    [HttpMethod.GET]: handleGET
});
export default handler;