import React, {FC, ReactElement, ReactNode, useCallback, useMemo, useState} from "react";
import {Tab, TabList, TabPanel, Tabs as ReactTabs, TabsProps} from "react-tabs";
import 'react-tabs/style/react-tabs.css';
import styled from "styled-components";
import PageWrapper from "./PageWrapper";

export type TabItem<K> = {
    key: K;
    name: string;
    content: ReactNode;
};

type Props<K> = {
   tabsProps?: TabsProps;
   children: TabItem<K>[];
   className?: string;
};
const Tabs = <K extends React.Key,>(
    {
        tabsProps,
        children,
        className
    }: Props<K>
) => {
    return (
        <Wrapper className={className}>
            <ReactTabs {...tabsProps} data-cy="ReactTabs">
                <TabListWrapper>
                    <StyledTabList data-cy="ReactTabs-TabList">
                        {
                            children.map((tab)=>
                                <Tab
                                    key={tab.key}
                                    data-cy={`ReactTabs-Tab-${tab.key}`}
                                >
                                    {tab.name}
                                </Tab>
                            )
                        }
                    </StyledTabList>
                </TabListWrapper>

                {
                    children.map((tab)=>
                        <TabPanel key={tab.key}>
                            {tab.content}
                        </TabPanel>
                    )
                }
            </ReactTabs>
        </Wrapper>
    );
};
export default Tabs;

export type UseTabsReturn<K> = {
    props: Omit<Props<K>, "children">;
    setTab: (key: K)=>void;
    tab: K;
};
export function useTabs<K extends React.Key>(
    getKeyByIndex: (index: number)=>K,
    getIndexByKey: (key: K)=>number,
    initKey?: K
): UseTabsReturn<K> {
    const [tabIndex, setTabIndex] = useState(initKey!==undefined ? getIndexByKey(initKey) : 0);

    const setTab = useCallback((key: K)=>{
        setTabIndex(getIndexByKey(key));
    }, [getIndexByKey]);

    return useMemo(()=>({
        props: {
            tabsProps: {
                selectedIndex: tabIndex
            }
        },
        setTab,
        tab: getKeyByIndex(tabIndex)
    }), [tabIndex, setTab, getKeyByIndex]);
}

const TabListWrapper = styled(PageWrapper)`
  border-bottom: 1px solid ${({theme})=>theme.colors.black};
  margin-bottom: ${({theme})=>theme.spacing(1)};
`;

const StyledTabList = styled(TabList)`
  margin: 0;
  padding: 0;
`;

const Wrapper = styled.div`
  & .react-tabs__tab {
    bottom: unset;
    border: 1px solid ${({theme})=>theme.colors.black};
    border-bottom: none;
    border-radius: ${({theme})=>theme.borderRadius} ${({theme})=>theme.borderRadius} 0 0;
    background-color: ${({theme})=>theme.colors.grey.main};
    padding: ${({theme})=>theme.spacing(0.7)} ${({theme})=>theme.spacing(2)};
    text-align: center;
    
    transition: ${({theme})=>theme.transition.generate(["background-color", "color"])};
    
    @media(hover: hover) {
      &:hover {
        background-color: ${({theme})=>theme.colors.primary};
        color: ${({theme})=>theme.colors.white};
      }
    }
    
    ${({theme})=>theme.media.up("md")} {
      width: 180px;
    }
  }

  & .react-tabs__tab--selected {
    background-color: transparent;
    
    &:focus::after {
      display: none;
    }
  }
`;
