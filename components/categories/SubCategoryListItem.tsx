import {FC} from "react";
import {CategoryResponse} from "../../apiProps/apiResponseTypes";
import styled from "styled-components";
import Link from "../Link";

type Props = {
    category: CategoryResponse;
};
const SubCategoryListItem: FC<Props> = (
    {
        category
    }
) => {
    return (
        <Wrapper href={`/category/${category.slug}`}>
            {category.name}
        </Wrapper>
    );
};
export default SubCategoryListItem;

const Wrapper = styled(Link)`
  background-color: ${({theme})=>theme.colors.grey.main};
  border: 2px solid ${({theme})=>theme.colors.grey.darker};
  padding: ${({theme})=>theme.spacing(1.5)} ${({theme})=>theme.spacing(1)};
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  text-decoration: none !important;
  
  transition: ${({theme})=>theme.transition.generate(["background-color", "border-color", "color"])};

  @media (hover: hover) {
    &:hover {
      background-color: ${({theme}) => theme.colors.primary};
      border-color: ${({theme}) => theme.colors.primary};
      color: ${({theme}) => theme.colors.white};
    }
  }
`;