import {FC, ReactNode, useCallback, useEffect} from "react";
import {CategoryResponse} from "../../apiProps/apiResponseTypes";
import Tabs, {useTabs} from "../Tabs";
import {useRouter} from "next/router";
import PageWrapper from "../PageWrapper";
import {useLoadingBar} from "../LoadingBar";

const FILTER_TABS_DATA: CategoryResponse[] = [
    {
        slug: "top",
        name: "TOP",
        id: 100
    },
    {
        slug: "best-seller",
        name: "Nejprodávanější",
        id: 101
    },
    {
        slug: "from-the-cheapest",
        name: "Od nejlevnějšího",
        id: 102
    },
    {
        slug: "from-the-most-expensive",
        name: "Od nejdražšího",
        id: 103
    }
];

type Props = {
    category: CategoryResponse;
    getContent: (category: CategoryResponse)=>ReactNode;
    className?: string;
};
const FilterTabs: FC<Props> = (
    {
        category,
        className,
        getContent
    }
) => {
    const router = useRouter();
    const getKeyByIndex = useCallback((index: number)=>FILTER_TABS_DATA[index].slug, []);
    const getIndexByKey = useCallback((key: string)=>
        FILTER_TABS_DATA.findIndex((category)=>category.slug===key),
        []);

    const tabs = useTabs(getKeyByIndex, getIndexByKey, router.query.filter as string | undefined);

    const onSelect = useCallback((index: number)=>{
        (async ()=>{
            const key: string = getKeyByIndex(index);
            // It may be loading here.
            await router.push(
                {
                    pathname: router.pathname,
                    query: {filter: key, slug: category.slug}
                },
                undefined,
                { scroll: false }
            )
        })();
    }, [getKeyByIndex, router, category]);

    useEffect(()=>{
        if (router.query.filter===undefined) return;
        tabs.setTab(router.query.filter as string);

        // Change only if the filter parameter is changed
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [router.query.filter]);

    return (
        <Tabs {...tabs.props} tabsProps={{...tabs.props.tabsProps, onSelect}} className={className}>
            {
                FILTER_TABS_DATA.map((filterCategory)=>({
                    key: filterCategory.slug,
                    name: filterCategory.name,
                    content: getContent(filterCategory)
                }))
            }
        </Tabs>
    );
};
export default FilterTabs;