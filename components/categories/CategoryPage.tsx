import {FC} from "react";
import CategoryHeading from "./CategoryHeading";
import SubCategoryList from "./SubCategoryList";
import {CategoryResponse, ProductResponse} from "../../apiProps/apiResponseTypes";
import TopSellingProducts from "../products/TopSellingProducts";
import styled from "styled-components";
import FilterTabs from "./FilterTabs";
import PageWrapper from "../PageWrapper";
import ProductsGrid from "../products/ProductsGrid";

type Props = {
    category: CategoryResponse;
    products: ProductResponse[];
};
const CategoryPage: FC<Props> = (
    {
        category,
        products
    }
) => {
    return (
        <>
            <PageWrapper>
                <CategoryHeading>{category.name}</CategoryHeading>
                <SubCategoryList categories={category.subCategories} />
                <StyledTopSellingProducts products={products} />
            </PageWrapper>
            <StyledFilterTabs category={category} getContent={()=><StyledProductsGrid products={products} />} />
        </>
    );
};
export default CategoryPage;

const StyledTopSellingProducts = styled(TopSellingProducts)`
  margin-top: ${({theme})=>theme.spacing(5)};
`;

const StyledFilterTabs = styled(FilterTabs)`
  margin-top: ${({theme})=>theme.spacing(5)};
`;

const StyledProductsGrid = styled(ProductsGrid)`
  margin-top: ${({theme})=>theme.spacing(4)};
`;