import {FC} from "react";
import {CategoryResponse} from "../../apiProps/apiResponseTypes";
import styled from "styled-components";
import SubCategoryListItem from "./SubCategoryListItem";

type Props = {
    categories?: CategoryResponse[];
};
const SubCategoryList: FC<Props> = (
    {
        categories
    }
) => {

    if (!categories) return null;

    return (
        <Wrapper>
            {
                categories.map((category)=>
                    <SubCategoryListItem
                        key={category.id}
                        category={category}
                    />
                )
            }
        </Wrapper>
    );
};
export default SubCategoryList;

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(1, 1fr);
  gap: ${({theme})=>theme.spacing(1)};
  
  ${({theme})=>theme.media.up("sm")} {
    grid-template-columns: repeat(2, 1fr);
  }
  ${({theme})=>theme.media.up("md")} {
    grid-template-columns: repeat(4, 1fr);
  }
  ${({theme})=>theme.media.up("lg")} {
    grid-template-columns: repeat(5, 1fr);
  }
`;

