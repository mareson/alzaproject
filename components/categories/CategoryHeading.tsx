import styled from "styled-components";


const CategoryHeading = styled.h1`
  color: ${({theme})=>theme.colors.primary};
  font-weight: normal;
  margin: ${({theme})=>theme.spacing(2)} 0 ${({theme})=>theme.spacing(3)} 0;
`;
export default CategoryHeading;