import {createGlobalStyle, css, DefaultTheme} from "styled-components";

const GlobalStyle = createGlobalStyle`
  html,
  body {
    padding: 0;
    margin: 0;
    font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen,
    Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
    ${({theme})=>theme.typography.generate("body")}
  }
  
  button {
    ${({theme})=>theme.typography.generate("body")}
  }
  
  a, a:link, a:visited {
    color: inherit;
    text-decoration: none;
  }
  @media(hover: hover) {
    a:hover {
    text-decoration: underline;
    }
  }
  
  * {
    box-sizing: border-box;
  }
  
  h1 {
    ${({theme})=>theme.typography.generate("h1")}
    margin: 0;
  }
  
  h2 {
    ${({theme})=>theme.typography.generate("h2")}
    margin: 0;
  }
`;

export default GlobalStyle;