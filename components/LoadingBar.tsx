import {createContext, FC, ReactNode, useCallback, useContext, useEffect, useRef, useState} from "react";
import {randomIntFromInterval} from "../helpers";
import styled from "styled-components";
import {AnimatePresence, motion} from "framer-motion";
import {useRouter} from "next/router";

interface LoadingBarContextProps {
    handleShow: ()=>void;
    handleHide: ()=>void;
}

export const LoadingBarContext = createContext<LoadingBarContextProps>({
    handleShow: ()=>{},
    handleHide: ()=>{}
});

const WIDTHS = [5, 10, 20, 30, 40, 50];

type Props = {
    children?: ReactNode;
};
const LoadingBar: FC<Props> = ({children}) => {
    const [show, setShow] = useState<boolean>(false);
    const [position, setPosition] = useState<{left: number; width: number;}>({left: 0, width: 50});
    const lastLeft = useRef<"left" | "right">("left");

    const change = () => {
        const width: number = WIDTHS[randomIntFromInterval(0, WIDTHS.length-1)];
        setPosition({left: lastLeft.current==="left" ? 100-width : 0, width});
        lastLeft.current = lastLeft.current==="left" ? "right" : "left";
    };

    const handleShow = useCallback(() => setShow(true), []);
    const handleHide = useCallback(() => setShow(false), []);

    const router = useRouter();

    useEffect(()=>{
        router.events.on("routeChangeStart", handleShow);
        router.events.on("routeChangeComplete", handleHide);
        return ()=>{
            router.events.off("routeChangeStart", handleShow);
            router.events.off("routeChangeComplete", handleHide);
        };
    }, [handleHide, handleShow]);

    return (
        <>
            <AnimatePresence>
            {
                show && (
                    <motion.div
                        initial={{opacity: 0}}
                        animate={{opacity: 1}}
                        exit={{opacity: 0}}
                    >
                        <Bar
                            animate={{left: `${position.left}%`, width: `${position.width}%`}}
                            transition={{type: "tween", ease: "easeInOut", duration: 1}}

                            onAnimationComplete={change}
                        />
                    </motion.div>
                )
            }
            </AnimatePresence>
            <LoadingBarContext.Provider value={{handleHide, handleShow}}>
                {children}
            </LoadingBarContext.Provider>
        </>
    )
};

export default LoadingBar;

export function useLoadingBar() {
    return useContext(LoadingBarContext);
}

const Bar = styled(motion.div)`
  position: fixed;
  top: 0; height: 5px;
  background-color: ${({theme})=>theme.colors.primary};
  z-index: 1000;
`;