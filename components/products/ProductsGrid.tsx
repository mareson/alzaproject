import {FC} from "react";
import {ProductResponse} from "../../apiProps/apiResponseTypes";
import ProductMiniature from "./ProductMiniature";
import PageWrapper from "../PageWrapper";
import styled, {css} from "styled-components";
import {BreakpointKey} from "../../styled";

type Props = {
    products: ProductResponse[];
    className?: string;
};
const ProductsGrid: FC<Props> = (
    {
        products,
        className
    }
) => {
    return (
        <PageWrapper className={className}>
            <ProductsWrapper data-cy="ProductsGrid">
                {
                    products.map((product)=>
                        <ProductMiniature
                            key={product.id}
                            product={product}
                        />
                    )
                }
            </ProductsWrapper>
        </PageWrapper>
    );
};
export default ProductsGrid;

export const PRODUCTS_SPACING: {
    [key in BreakpointKey]: number
} = {
    xs: 2,
    sm: 2,
    md: 2,
    lg: 4,
    xl: 4
}; // factor

const ProductsWrapper = styled.div`
  display: grid;
  place-items: center;
  gap: ${({theme})=>theme.spacing(2)};
  
  ${Object.entries(PRODUCTS_SPACING).map(([key, value])=>css`
    ${({theme})=>theme.media.up(key as BreakpointKey)} {
      gap: ${({theme})=>theme.spacing(value)};
    }
  `)}

  grid-template-columns: repeat(auto-fill, minmax(160px, 1fr));
  ${({theme})=>theme.media.up("md")} {
    grid-template-columns: repeat(auto-fill, minmax(190px, 1fr));
  }
`;