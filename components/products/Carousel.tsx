import {FC, useMemo, useRef} from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import ProductMiniature from "./ProductMiniature";
import styled, {useTheme} from "styled-components";
import SwiperCore, {SwiperOptions} from 'swiper';
import NoSSR from "react-no-ssr";
import {ProductResponse} from "../../apiProps/apiResponseTypes";
import {PRODUCTS_SPACING} from "./ProductsGrid";
import {BreakpointKey} from "../../styled";

const CAROUSEL_SLIDES_PER_VIEW: {
    [key in BreakpointKey]: number
} = {
    xs: 2,
    sm: 3,
    md: 4,
    lg: 5,
    xl: 5
};

type Props = {
    products: ProductResponse[];
};
const Carousel: FC<Props> = (
    {
        products
    }
) => {
    const swiperRef = useRef<SwiperCore>();
    const theme = useTheme();

    const breakpoints = useMemo((): SwiperOptions["breakpoints"] => {
        const result: SwiperOptions["breakpoints"] = {};

        Object.entries(PRODUCTS_SPACING).forEach(([key, value]) => {
            result[theme.breakpoints[key as BreakpointKey]] = {
                spaceBetween: theme.basicSpacing * value,
                slidesPerView: CAROUSEL_SLIDES_PER_VIEW[key as BreakpointKey]
            };
        });

        return result;
    }, [theme]);

    return (
        <NoSSR>
            <Wrapper>
                <Navigation>
                    <div onClick={()=>swiperRef.current?.slidePrev()}>&lt;</div>
                </Navigation>
                <StyledSwiper
                    slidesPerView={2}
                    spaceBetween={theme.basicSpacing*2}
                    loop

                    onBeforeInit={(swiper)=>{
                        swiperRef.current = swiper;
                    }}

                    breakpoints={breakpoints}
                >
                    {
                        products.map(
                            (product)=>
                                <StyledSwiperSlide key={product.id}>
                                    <ProductMiniature product={product} lightVariant />
                                </StyledSwiperSlide>
                        )
                    }
                </StyledSwiper>
                <Navigation>
                    <div onClick={()=>swiperRef.current?.slideNext()}>&gt;</div>
                </Navigation>
            </Wrapper>
        </NoSSR>
    );
};
export default Carousel;

const Wrapper = styled.div`
  display: flex;
`;

const StyledSwiper = styled(Swiper)`
  flex-grow: 1;
`;

const StyledSwiperSlide = styled(SwiperSlide)`
  display: flex;
  justify-content: center;
`;

const NAVIGATION_MARGIN: number = 1; // factor
const Navigation = styled.div`
  display: none;
  align-items: center;
  
  ${({theme})=>theme.media.up("sm")} {
    display: grid;
  }
  
  & > div {
    background-color: ${({theme})=>theme.colors.secondary};
    color: ${({theme})=>theme.colors.white};
    padding: ${({theme})=>theme.spacing(2)} ${({theme})=>theme.spacing(0.5)};
    border: 1px solid ${({theme})=>theme.colors.black};
    border-radius: ${({theme})=>theme.borderRadius};
    user-select: none;
    cursor: pointer;
    
    transition: ${({theme})=>theme.transition.generate(["background-color"])};
    
    @media(hover: hover) {
      &:hover {
        background-color: ${({theme})=>theme.colors.primary};
      }
    }
  }
  
  &:first-of-type {
    margin-right: ${({theme})=>theme.spacing(NAVIGATION_MARGIN)};
  }
  &:last-of-type {
    margin-left: ${({theme})=>theme.spacing(NAVIGATION_MARGIN)};
  }
`;