import {FC} from "react";
import styled from "styled-components";
import Carousel from "./Carousel";
import {ProductResponse} from "../../apiProps/apiResponseTypes";
import NoSSR from "react-no-ssr";

type Props = {
    className?: string;
    products: ProductResponse[];
};
const TopSellingProducts: FC<Props> = (
    {
        className,
        products
    }
) => {
    return (
        <Wrapper className={className}>
            <Heading>Nejprodávanější</Heading>
            <Carousel products={products} />
        </Wrapper>
    );
};
export default TopSellingProducts;

const Wrapper = styled.div`

`;

const Heading = styled.h2`
  margin-bottom: ${({theme})=>theme.spacing(1)};
`;