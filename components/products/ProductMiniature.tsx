import {FC} from "react";
import styled, {css} from "styled-components";
import Image from "next/image";
import {ProductResponse} from "../../apiProps/apiResponseTypes";
import {Rating} from "react-simple-star-rating";
import DynamicButton from "../DynamicButton";
import NoSSR from "react-no-ssr";

type CommonProps = {
    lightVariant?: boolean;
}

type Props = {
    product: ProductResponse;
} & CommonProps;
const ProductMiniature: FC<Props> = (
    {
        product,
        lightVariant
    }
) => {

    const image = (
        <Image
            src={product.img}
            layout="fill"
            alt="product"
            objectFit="cover"
            objectPosition="center center"
        />
    );

    const productName = <ProductName>{product.name}</ProductName>;

    const ratingWrapper = (
        <RatingWrapper lightVariant={lightVariant}>
            <NoSSR> {/* Because the server side does not match the client side of the Rating library. */}
                <Rating
                    initialValue={product.rating}
                    allowHalfIcon
                    readonly
                    ratingValue={0}
                    size={lightVariant ? 30 : 25}
                />
            </NoSSR>
        </RatingWrapper>
    );

    const productDescription = <ProductDescription lightVariant={lightVariant}>{product.spec}</ProductDescription>;

    if (lightVariant)
        return (
            <Wrapper>
                <ImageWrapper>
                    {image}
                </ImageWrapper>
                {productName}
                {ratingWrapper}
                {productDescription}
                <PricingWrapper>{product.price}</PricingWrapper>
            </Wrapper>
        );

    return (
        <Wrapper data-cy="ProductMiniature">
            {productName}
            {productDescription}
            <ImageWrapper>
                {image}
                {ratingWrapper}
            </ImageWrapper>
            <PricingWrapper>
                <div>
                    <div>{product.price}</div>
                    <SubPrice strikethrough>{product.price}</SubPrice>
                </div>
                <div>
                    <DynamicButton
                        label="Koupit"
                    />
                </div>
            </PricingWrapper>
            <Stock>
                {product.avail}
            </Stock>
        </Wrapper>
    );
};
export default ProductMiniature;

const Wrapper = styled.div`
  max-width: 190px;
  width: 100%;
  display: flex;
  flex-direction: column;
  gap: ${({theme})=>theme.spacing(0.8)};
`;

const ImageWrapper = styled.div`
  width: 100%;
  position: relative;
  
  &:before {
    content:'';
    float:left;
    padding-top: 100%;
  }
`;

const ProductName = styled.div`
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
`;

type RatingWrapperProps = CommonProps;
const RatingWrapper = styled.div<RatingWrapperProps>`
  ${({lightVariant})=>!lightVariant && css`
    position: absolute;
    bottom: 0;
    left: 0;
  `}
`;

const DESCRIPTION_LINE_HEIGHT: number = 15; // px – because of safari, we have to use pixels

type ProductDescriptionProps = CommonProps;
const ProductDescription = styled.div<ProductDescriptionProps>`
  ${({theme})=>theme.typography.generate("description")}

  display: -webkit-box;
  -webkit-box-orient: vertical;
  white-space: normal;
  text-overflow: ellipsis;
  overflow: hidden;
  line-height: ${DESCRIPTION_LINE_HEIGHT}px;
  
  ${
    ({lightVariant})=>lightVariant 
      ? css`
        height: ${DESCRIPTION_LINE_HEIGHT*3}px;
        -webkit-line-clamp: 3;
      ` 
      : css`
        height: ${DESCRIPTION_LINE_HEIGHT*9}px;
        -webkit-line-clamp: 9;
        color: ${({theme})=>theme.colors.grey.dark};
      `
  }
`;

type PricingWrapperProps = CommonProps;
const PricingWrapper = styled.div<PricingWrapperProps>`
  color: ${({theme})=>theme.colors.info};
  
  ${({lightVariant})=>!lightVariant && css`
    display: flex;
    justify-content: space-between;
    align-items: center;
    gap: ${({theme})=>theme.spacing(0.4)};
  `}
`;

type SubPriceProps = {
    strikethrough?: boolean;
};
const SubPrice = styled.div<SubPriceProps>`
  font-size: ${({theme})=>theme.typography.description.fontSize};
  color: ${({theme})=>theme.colors.black};
  position: relative;
  
  ${({strikethrough})=>strikethrough && css`
    &:after {
      content: "";
      position: absolute;
      top: calc(50% - 0.5px);
      left: 0;
      height: 1px;
      width: 100%;
      background-color: ${({theme})=>theme.colors.black};
      transform: rotate(-10deg);
    }
  `}
`;

const Stock = styled.div`
  text-align: center;
  font-size: ${({theme})=>theme.typography.description.fontSize};
  font-weight: bold;
`;