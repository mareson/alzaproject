import {FC, ReactNode} from "react";
import NextLink, {LinkProps as NextLinkProps} from 'next/link'

export type LinkProps = {
    href: string;
    nextLinkProps?: Partial<NextLinkProps>;
    children: ReactNode;
    className?: string;
}

const Link: FC<LinkProps> = (
    {
        href,
        nextLinkProps,
        children,
        className
    }
) => {
    return (
        <NextLink href={href} {...nextLinkProps}>
            <a className={className} data-cy="link">{children}</a>
        </NextLink>
    );
};
export default Link;