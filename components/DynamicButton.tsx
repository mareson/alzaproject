import {FC, Key, MouseEventHandler, ReactNode, useEffect, useRef, useState} from "react";
import styled, {css} from "styled-components";

const MENU_WIDTH: number = 150; // px

const ITEMS: {
    key: Key,
    name: string;
}[] = [
    {
        key: 1,
        name: "Koupit zrychleně"
    },
    {
        key: 2,
        name: "Porovnat"
    },
    {
        key: 3,
        name: "Hlídat"
    },
    {
        key: 4,
        name: "Přidat do seznamu"
    }
];

type Props = {
    label?: ReactNode;
};
const DynamicButton: FC<Props> = (
    {
        label
    }
) => {
    const buttonRef = useRef<HTMLButtonElement>(null);
    const menuRef = useRef<HTMLSpanElement>(null);
    const [isOpen, setIsOpen] = useState<boolean>(false);

    const toggleMenu: MouseEventHandler<HTMLButtonElement> = (e) => {
        e.stopPropagation();

        if (!isOpen && buttonRef.current && menuRef.current) {
            const buttonEl = buttonRef.current;
            const menuEl = menuRef.current;
            const endOfMenu = buttonEl.offsetLeft + MENU_WIDTH;

            if (endOfMenu>window.innerWidth) {
                menuEl.style.left = `-${MENU_WIDTH - buttonEl.offsetWidth}px`;
            } else {
                menuEl.style.left = "0px";
            }
        }

        setIsOpen(!isOpen);
    };

    useEffect(()=>{
        const clickAwayListener = () => {
            setIsOpen(false);
        };

        window.addEventListener("click", clickAwayListener);
        return ()=>window.removeEventListener("click", clickAwayListener);
    }, []);

    return (
        <Wrapper onClick={toggleMenu} ref={buttonRef}>
            <Label>{label}</Label>
            <Arrow>{!isOpen ? <>&#9660;</> : <>&#9650;</>}</Arrow>
            <Menu isOpen={isOpen} ref={menuRef}>
                {
                    ITEMS.map((item)=>
                        <MenuItem
                            key={item.key}
                            onClick={e=>e.stopPropagation()}
                        >
                            {item.name}
                        </MenuItem>
                    )
                }
            </Menu>
        </Wrapper>
    );
};
export default DynamicButton;

const Wrapper = styled.button`
  appearance: none;
  border: 1px solid ${({theme})=>theme.colors.black};
  border-radius: ${({theme})=>theme.borderRadius};
  background-color: transparent;
  position: relative;
  padding: 0;
  display: flex;
  gap: ${({theme})=>theme.spacing(1)};
  align-items: center;
  cursor: pointer;
  transition: ${({theme})=>theme.transition.generate(["background-color"])};
  
  @media(hover: hover) {
    &:hover {
      background-color: ${({theme})=>theme.colors.grey.main};
    }
  }
`;

const Label = styled.span`
  padding: ${({theme})=>theme.spacing(0.2)} ${({theme})=>theme.spacing(0.5)};
  font-size: ${({theme})=>theme.typography.description.fontSize};
`;

const Arrow = styled.span`
  background-color: ${({theme})=>theme.colors.grey.main};
  padding: ${({theme})=>theme.spacing(0.2)} ${({theme})=>theme.spacing(0.5)};
  border-left: 1px solid ${({theme})=>theme.colors.black};
  border-top-right-radius: ${({theme})=>theme.borderRadius};
  border-bottom-right-radius: ${({theme})=>theme.borderRadius};
`;

type MenuProps = {
    isOpen?: boolean;
};
const Menu = styled.span<MenuProps>`
  position: absolute;
  top: 100%;
  left: 0;
  width: ${MENU_WIDTH}px;
  flex-direction: column;
  display: none;
  z-index: 1;
  background-color: ${({theme})=>theme.colors.white};
  border: 1px solid ${({theme})=>theme.colors.grey.darker};
  border-radius: ${({theme})=>theme.borderRadius};
  
  ${({isOpen})=>isOpen && css`
    display: flex;
  `}
`;

const MenuItem = styled.span`
  padding: ${({theme})=>theme.spacing(0.5)} ${({theme})=>theme.spacing(1)};
  border-bottom: 1px solid ${({theme})=>theme.colors.grey.main};
  cursor: pointer;
  &:last-of-type {
    border-bottom: none;
  }
  
  transition: ${({theme})=>theme.transition.generate(["background-color"])};
  
  @media(hover: hover) {
    &:hover {
      background-color: ${({theme})=>theme.colors.grey.main};
    }
  }
`;