import styled from "styled-components";
import {THEME} from "../theme";
import {FC, ReactNode} from "react";

type Props = {
    children?: ReactNode;
    className?: string;
};
const PageWrapper: FC<Props> = (
    {
        children,
        className
    }
) => {
    return (
        <Wrapper className={className}>
            <InnerWrapper>{children}</InnerWrapper>
        </Wrapper>
    );
};
export default PageWrapper;

const Wrapper = styled.section`
  display: flex;
  justify-content: center;
`;

const InnerWrapper = styled.div`
  padding: 0 ${({theme})=>theme.spacing(theme.pageSideMargins)};
  max-width: ${({theme})=>theme.pageMaxWidth}px;
  width: 100%;
`;