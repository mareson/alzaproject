import {FC} from "react";
import styled from "styled-components";
import PageWrapper from "./PageWrapper";

type Props = {};
const Footer: FC<Props> = () => {
    return (
        <Wrapper>
            <PageWrapper>
                &copy; 2022 Ondřej Mareš
            </PageWrapper>
        </Wrapper>
    );
};
export default Footer;

const Wrapper = styled.footer`
  background-color: ${({theme})=>theme.colors.primary};
  color: ${({theme})=>theme.colors.white};
  margin-top: ${({theme})=>theme.spacing(4)};
  padding: ${({theme})=>theme.spacing(4)} 0;
`;