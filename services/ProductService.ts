import {RequestService} from "./RequestService";
import {ProductResponse, ProductsResponse} from "../apiProps/apiResponseTypes";
import {HttpMethod} from "../apiProps/apiHandler";

class ProductService extends RequestService {
    public retrieveProducts(): Promise<ProductResponse[]> {
        return new Promise(async (resolve, reject)=>{
            try {
                const responseData = await this.performRequest<ProductsResponse>("/products", HttpMethod.GET);
                resolve(responseData.data);
            } catch (e) {
                console.error(e);
                reject(e);
            }
        });
    }
}

const productService: ProductService = new ProductService();
export default productService;