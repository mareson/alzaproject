import {RequestService} from "./RequestService";
import {AlzaProductsResponse} from "../apiProps/externalResponseTypes";
import {ApiError} from "../apiProps/apiErrors";
import {HttpMethod} from "../apiProps/apiHandler";

export const ALZA_PRODUCTS_URL = "https://www.alza.cz/Services/RestService.svc/v2/products";

export class AlzaService extends RequestService {
    public retrieveProducts(): Promise<AlzaProductsResponse> {
        return new Promise(async (resolve, reject)=>{
            try {
                const responseData = await this.performRequest<AlzaProductsResponse>(
                    ALZA_PRODUCTS_URL,
                    HttpMethod.POST,
                    {
                        "filterParameters": {
                            "id": 18855843,
                            "isInStockOnly": false,
                            "newsOnly": false,
                            "wearType": 0,
                            "orderBy": 0,
                            "page": 1,
                            "params": [{
                                "tId": 0,
                                "v": []
                            }],
                            "producers": [],
                            "sendPrices": true,
                            "type": "action",
                            "typeId": "",
                            "branchId": ""
                        }
                    },
                    {
                        external: true
                    }
                );

                if (responseData.err>0) {
                    reject(ApiError.ALZA_REQUEST_FAILED);
                } else {
                    resolve(responseData);
                }
            } catch (e) {
                console.error(e);
                reject(ApiError.ALZA_REQUEST_FAILED);
            }
        });
    }
}

const alzaService: AlzaService = new AlzaService();
export default alzaService;