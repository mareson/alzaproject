import {RequestInit} from "next/dist/server/web/spec-extension/request";
import {HttpMethod} from "../apiProps/apiHandler";

const API_URL = (process.env.APP_URL ?? "")+"/api";

export type RequestProps = {
    init?: Partial<RequestInit>;
    external?: boolean;
}

export class RequestService {
    protected async performRequest<RD, D = Object>(
        url: string,
        method: HttpMethod,
        data?: D,
        requestProps?: RequestProps
    ): Promise<RD> {

        const headers: HeadersInit = {};
        if (!!data) headers["Content-Type"] = "application/json";

        const response = await fetch(requestProps?.external ? url : API_URL+url, {
            method,
            headers,
            body: !!data ? JSON.stringify(data) : undefined,
            ...requestProps?.init
        });

        if (response.ok) {
            try {
                const resultData: RD = await response.json();
                return resultData;
            } catch (e) {}
        }

        throw response;
    }
}

const requestService: RequestService = new RequestService();
export default requestService;

