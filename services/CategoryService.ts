import {RequestService} from "./RequestService";
import {CategoryResponse} from "../apiProps/apiResponseTypes";
import dataLaptops from "../dataLaptops";

class CategoryService extends RequestService {
    public async retrieveCategoryBySlug(slug: string): Promise<CategoryResponse> {
        return dataLaptops;
    }
}

const categoryService: CategoryService = new CategoryService();
export default categoryService;