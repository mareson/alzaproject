
export enum Message {

    // API MESSAGES
    UNSUPPORTED_METHOD,
    ALZA_REQUEST_FAILED
}

export const MessageContent_CS: {
    [key in Message]: string
} = {

    // API MESSAGES
    [Message.UNSUPPORTED_METHOD]: "Nepodporovaná metoda",
    [Message.ALZA_REQUEST_FAILED]: "Nepodařilo se získat data z Alzy"
}