
const link = "[data-cy='link']";
const productsMiniature = "[data-cy='ProductMiniature']";

describe("Products listing", ()=>{
    it("renders products listing correctly", ()=>{
        cy.visit("/");
        cy.get(link).click();
        cy.location('pathname', {timeout: 60000}).should('include', '/category');
        cy.wait(60000).get(productsMiniature).should("have.length.gt", 0,);
    });
});