import Tabs from "../../components/Tabs";
import {Key} from "react";

const reactTabs = "[data-cy=ReactTabs]";
const reactTabsList = "[data-cy='ReactTabs-TabList']";
const getTabIdentifier = (key: Key) => `[data-cy='ReactTabs-Tab-${key}']`;

const tabsData = [
    {
        key: "test1",
        name: "Test 1",
        content: <>Content 1</>
    },
    {
        key: "test2",
        name: "Test 2",
        content: <>Content 2</>
    }
];

describe("<Tabs>", ()=>{
    it("mounts", ()=>{
        cy.mount(<Tabs>{[]}</Tabs>);
    });

    it("renders tabs correctly", ()=>{
        cy.mount(
            <Tabs>{tabsData}</Tabs>
        );

        cy.get(reactTabsList).should("contain", "Test 1").should("contain", "Test 2");
    });

    it("changes tab correctly", ()=>{
        cy.mount(
            <Tabs>{tabsData}</Tabs>
        );

        cy.get(getTabIdentifier("test2")).click();
        cy.get(reactTabs).should("contain", "Content 2");
        cy.get(getTabIdentifier("test1")).click();
        cy.get(reactTabs).should("contain", "Content 1");
    });
})