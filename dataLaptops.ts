import {CategoryResponse} from "./apiProps/apiResponseTypes";

const laptopsCategory: CategoryResponse = {
    id: 1,
    name: "Notebooky",
    slug: "laptops",
    subCategories: [
        {id: 2, name: "Macbook", slug: "macbook"},
        {id: 3, name: "Herní", slug: "gaming"},
        {id: 4, name: "Kancelářské", slug: "office"},
        {id: 5, name: "Profesionální", slug: "professional"},
        {id: 6, name: "Stylové", slug: "stylish"},
        {id: 7, name: "Základní", slug: "basic"},
        {id: 8, name: "Dotykové", slug: "touchable"},
        {id: 9, name: "Na splátky", slug: "installments"},
        {id: 10, name: "VR Ready", slug: "vr-ready"},
        {id: 11, name: "IRIS Graphics", slug: "iris-graphics"},
        {id: 12, name: "Brašný, batohy", slug: "bags"},
        {id: 13, name: "Příslušenství", slug: "accessories"}
    ]
};
export default laptopsCategory;