
export interface AlzaResponse {}

export interface AlzaProductResponse extends AlzaResponse {
    id: number,
    code: string,
    img: string, // https://i.alza.cz/Foto/f8/ES/ESO2557n4.jpg
    name: string, // Philips Hue White 8.5W E27 starter kit
    spec: string, // LED žárovka 2x LED 8.5W, A60, patice E27, 25000 hodin, 2700K, stmívatelná, ovládání pomocí chytrých zařízení, + Hue Bridge
    price: string, // 2&nbsp;199 Kč
    cprice: null | string,
    priceWithoutVat: string, // 2&nbsp;199 Kč
    avail: string, // Skladem &gt; 5&nbsp;ks
    rating: number, // float <0;5>
    order: number,
    url: string, // https://www.alza.cz/philips-hue-9-5w-a60-e27-set-d4051892.htm?catid=18855843
    minimumAmount: number,
    amountInPack: number
}

export interface AlzaProductsResponse extends AlzaResponse {
    err: number,
    msg: null | string,
    data: AlzaProductResponse[]
}