import {AlzaProductResponse, AlzaProductsResponse} from "./externalResponseTypes";

export interface ApiResponse {}

export interface ListResponse<E extends ApiResponse> extends ApiResponse {
    data: E[]
}

export interface ErrorResponse extends ApiResponse {
    message?: string;
    status: number;
}


// PRODUCTS

export interface ProductResponse extends ApiResponse, AlzaProductResponse {}

export interface ProductsResponse extends ListResponse<ProductResponse> {}


// CATEGORIES

export interface CategoryResponse extends ApiResponse {
    id: number;
    name: string;
    slug: string;
    subCategories?: CategoryResponse[];
}